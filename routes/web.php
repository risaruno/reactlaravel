<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', "MessageController@index");
Route::get('chatroom', "MessageController@chatroom");
Route::get('message', "MessageController@fetch");
Route::post('message', "MessageController@send");