<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

	public $table = "message";

	protected $fillable = ['id', 'name', 'message'];

	function user() {

		return $this->belongsTo(User::class);

	}
}
