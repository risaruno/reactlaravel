<?php

namespace App\Http\Controllers;

use Auth;
use App\Message;
use Illuminate\Http\Request;
use App\Events\SendMessage;

class MessageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('welcome');
    }

    public function chatroom()
    {
        return view('chatroom');
    }

    public function fetch()
    {
    	return Message::get();
    }

    public function send(Request $request)
    {
    	$user = Auth::user();

    	$message = $user->message()->create([
    		'name' => $user->name,
    		'message' => $request->input('message'),

    	]);

		broadcast(new SendMessage($user, $message))->toOthers();

		return [
            'name' => $user->name,
            'message' => $message->message,
        ];
    }

}
