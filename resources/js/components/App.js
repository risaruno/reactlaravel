import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import "../app.css";
import Pusher from "pusher-js";
import axios from "axios";
import ChatList from "./ChatList";
import { ChatProvider } from "./ChatContext.js";
import Users from "./Users.js";
import ChatForm from "./ChatForm.js";

const App = () => {
    return (
        <ChatProvider>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-3">
                        <div className="card userlist">
                            <div className="card-header">User List</div>
                            <div className="card-body">
                                {/* {datas.map(data => (
                                <Users user={data.name} />
                            ))} */}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-9">
                        <div className="card chatroom">
                            <div className="card-header">Chat Room</div>
                            <div className="card-body">
                                <div className="chats">
                                    <ChatList />
                                </div>
                                <div className="message">
                                    <ChatForm />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ChatProvider>
    );
};

export default App;

if (document.getElementById("room")) {
    ReactDOM.render(<App />, document.getElementById("room"));
}
