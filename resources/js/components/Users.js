import React from "react";

const Users = ({ user }) => {
    var auth = document
        .querySelector('meta[name="user"]')
        .getAttribute("content");

    return (
        <div>
            <span className={user == auth ? "user me" : "user"}>{user}</span>
        </div>
    );
};

export default Users;
