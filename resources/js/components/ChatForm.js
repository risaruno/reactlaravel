import React, { useState, useContext } from "react";
import { ChatContext } from "./ChatContext";

const ChatForm = () => {
    const [msg, setMsg] = useState([]);
    const [datas, setDatas] = useContext(ChatContext);

    const msgChange = event => {
        setMsg({name: auth, message: event.target.value});
    };

    const submitForm = async event => {
        event.preventDefault();
        const response = await axios.post("./message", msg)
        .catch(error => {console.error(error)});
        console.log(msg);
        //setDatas(msg);
        //event.target.querySelector('inputMsg').value = '';
    };

    var token = document
        .querySelector('meta[name="csrf-token"]')
        .getAttribute("content");

    var auth = document
        .querySelector('meta[name="user"]')
        .getAttribute("content");

    return (
        <form id="msgForm" onSubmit={submitForm} method="post">
            <input type="hidden" name="_token" value={token} />
            <input
                className="inputMsg"
                type="text"
                name="message"
                onChange={msgChange}
            />
            <button className="btnMsg" type="submit">
                Send
            </button>
        </form>
    );
};

export default ChatForm;
