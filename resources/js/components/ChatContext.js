import React, { useEffect, useState, createContext } from "react";

export const ChatContext = createContext();

export const ChatProvider = props => {
    const [datas, setDatas] = useState([]);

    useEffect(() => {
        getData();
    }, []);

    const getData = async () => {
        const response = await fetch("./message");
        const data = await response.json();
        console.log(data);
        setDatas(data);
    };

    var auth = document
        .querySelector('meta[name="user"]')
        .getAttribute("content");

    return (
        <ChatContext.Provider value={[datas, setDatas]}>
            {props.children}
        </ChatContext.Provider>
    );
};
