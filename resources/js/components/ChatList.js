import React, { useContext } from "react";
import { ChatContext } from "./ChatContext";

const ChatList = () => {
    const [datas, setDatas] = useContext(ChatContext);

    var auth = document
        .querySelector('meta[name="user"]')
        .getAttribute("content");

    return (
        <>
            {datas.map(data => (
                <div className={data.name == auth ? "chat me" : "chat"} key={data.id}>
                    <span>{data.name}</span>
                    <span>{data.message}</span>
                </div>
            ))}
        </>
    );
};

export default ChatList;
